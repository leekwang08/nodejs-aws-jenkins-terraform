terraform {
  backend "s3" {
    bucket = "kplee-node-aws-jenkins-terraform"
    key    = "node-aws-jenkins-terraform.tfstate"
    region = "us-east-1"
  }
}